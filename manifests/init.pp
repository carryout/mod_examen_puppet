class mod_examen_puppet{

#ficheros varios
file { '/var/www/prod/index.php':
    ensure => file,
    replace => true,
    content => "Hello world. Sistema operativo ${operatingsystem} version ${operatingsystemrelease} ",
    require => File ["/var/www/prod"]
  }
  file { "/var/www/prod":
    ensure => "directory",
  }
  
  file { '/var/www/dev/info.php':
    ensure => file,
    replace => true,
    content => "<?php phpinfo();>",
    require => File ["/var/www/dev"]

     
  }
  file { "/var/www/dev":
    ensure => "directory",
  }


class { 'apache':}
apache::vhost {'myMpwar.prod':
	port	=> '80',
	docroot	=> '/var/www/prod',
	docroot_owner => 'vagrant',
	docroot_group => 'vagrant',
    } 
apache::vhost {'myMpwar.dev':
	port	=> '80',
	docroot	=> '/var/www/dev',
	docroot_owner => 'vagrant',
	docroot_group => 'vagrant',
    }
    
class { '::mysql::server':
    databases => {
      'mpwar_test' => {
        ensure  => 'present',
        charset => 'utf8',
      },
       'mympwar' => {
        ensure  => 'present',
        charset => 'utf8',
      },
      }
}



# PHP
$php_version = '55'

# remi_php55 requires the remi repo as well
if $php_version == '55' {
  $yum_repo = 'remi-php55'
  include ::yum::repo::remi_php55
}
class { 'php':
  version => 'latest',
  require => Yumrepo[$yum_repo]
}

php::module { [ 'devel', 'pear', 'mbstring', 'xml', 'tidy', 'pecl-memcache', 'soap' ]: }

# memcached
include memcached


#iptables puerto 80
class{ 'iptables' :  }
iptables::rule { '80': port => '80', protocol => 'tcp',  }

#hosts
host { 'localhost':
  ensure => 'present',
  target => '/etc/hosts',
  ip => '127.0.0.1',
  host_aliases => ['myMpwar.prod', 'myMpwar.dev']
}


# Ensure Time Zone and Region.
class { 'timezone':
    timezone => 'Europe/Madrid',
}

#NTP
class { '::ntp':
  servers => [ '1.es.pool.ntp.org', '2.europe.pool.ntp.org', '3.europe.pool.ntp.org' ],
  package_ensure => 'latest'
}
}